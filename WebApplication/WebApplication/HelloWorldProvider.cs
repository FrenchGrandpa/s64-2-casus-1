﻿namespace WebApplication {

    public static class HelloWorldProvider {

        public static string GetHelloWorld(this string str) {
            return "Hello World";
        }

    }

}