using WebApplication;
using Xunit;


namespace TestProject1 {

    public class UnitTest1 {

        [Fact]
        public void HelloWorldTest() {
            Assert.Equal("Hello World", "Hoi".GetHelloWorld());
            Assert.NotEqual("hello world", "Hoi".GetHelloWorld());
        }

    }

}